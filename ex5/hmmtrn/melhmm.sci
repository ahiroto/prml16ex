function model = melhmm(gauss, occ, tra)

  ns = size(gauss,2); // Number of States

  model.state = cell(1,ns);
  model.trans = zeros(ns,2);

  for s=1:ns,
    tg = gauss(1,s).entries;
    tg.Mean = tg.Mean / occ(s,1)
    tg.Cov  = tg.Cov / occ(s,1)
    model.state(1,s).entries = tg;

    model.trans(s,1) = tra(s,1) / occ(s,1);
    model.trans(s,2) = 1 - model.trans(s,1);
  end

  return;

endfunction
