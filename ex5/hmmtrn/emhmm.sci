function model = emhmm(trnd, options)

if ~mtlb_isfield(options, 'nstate'), options.nstate = 3; end
if ~mtlb_isfield(options, 'tmax'), options.tmax = %inf; end
if ~mtlb_isfield(options, 'eps_logL'), options.eps_logL = 0; end
if ~mtlb_isfield(options, 'cov_type'), options.cov_type = 'full'; end
if ~mtlb_isfield(options, 'verb'), options.verb = 0; end

//------------------------------------------------------------
// mlchmm: calc initial estimates
//------------------------------------------------------------
model = mlchmm(trnd, options);

nd = size(trnd,2);           // Number of training data
ns = size(model.state,2); // Number of HMM states

// fp [cell(1,ndata)]: forward prob.
// bp [cell(1,ndata)]: backward prob.
// logObsProb [cell(1,ndata)]: log observation prob.
// logPr [1 x ndata]: total log prob.
[fp, bp, logObsProb, logPr] = calc_fb(trnd, model);
totalLogPr = sum(logPr(1,:)) / nd;

model.logPr = totalLogPr;
model.t = 1;
model.options = options;
model.fun = 'pdfgauss';

mprintf('Init: logPr=%f\n', model.logPr);

//--------------------------------------------------
// Main loop of EM algorithm
//--------------------------------------------------
model.exitflag = 0;
while model.exitflag == 0 & model.t <= options.tmax,

  //------------------------------------------------------------
  // Accumulate statistics
  //------------------------------------------------------------
  gauss = cell(1,ns);
  for s=1:ns,
    state = model.state(1,s).entries;
    tg.Mean = zeros(size(state.Mean,1), size(state.Mean,2));
    tg.Cov  = zeros(size(state.Cov,1), size(state.Cov,2));
    gauss(1,s).entries = tg;
  end
  occ = zeros(ns,1); // [nstate x 1]: sum of psi(t,i)
  tra = zeros(ns,1); // [nstate x 1]: sum of xi(t,i,i)

  //--------------------------------------------------
  for n=1:nd,
    X = trnd(1,n).entries; // [dim x nframe]: training data.
    [dim,nf] = size(X);

    lobsprob = logObsProb(1,n).entries; // [nstate x nframe]

    a = fp(1,n).entries; // [nstate x nframe]
    b = bp(1,n).entries; // [nstate x nframe]

    pab = -logPr(1,n) * ones(ns,nf) + a + b; // [nstate x nframe]: psi(t,i)
    pab = exp(pab);

    traPr = zeros(ns,nf-1); // [nstate,nframe-1]: xi(t,i,i)
    for s=1:ns,
      state = model.state(1,s).entries;
      XC = X - state.Mean * ones(1,nf);

      tg = gauss(1,s).entries;
      tg.Mean = tg.Mean + sum((repmat(pab(s,:),dim,1).*X),2);
      C = ((ones(dim,1) * pab(s,:)) .* XC) * XC';
      select options.cov_type,
        case 'full'
          tg.Cov = tg.Cov + (XC.*(repmat(pab(s,:),dim,1))*XC');
        case 'diag'
          tg.Cov = tg.Cov + diag(sum(XC.*(ones(dim,1)*pab(s,:)).*XC,2));
        else
          error('Wrong cov_type.');
      end
      gauss(1,s).entries = tg;

      for t=1:nf-1,
        traPr(s,t) = -logPr(1,n) + a(s,t) + log(model.trans(s,1)) + lobsprob(s,t+1) + b(s,t+1);
      end
    end
    occ = occ + sum(pab,2);
    tra = tra + sum(exp(traPr),2);
  end // n=1:nd

  //------------------------------------------------------------
  // melhmm: calc new estimates using acc values, occ and tra
  //------------------------------------------------------------
  tmp_model = melhmm(gauss, occ, tra);

  [fp, bp, logObsProb, logPr] = calc_fb(trnd, tmp_model);
  totalLogPr = sum(logPr(1,:)) / nd;

  tmp_model.logPr = totalLogPr;
  tmp_model.t = model.t;
  tmp_model.fun = 'pdfgauss';

  difLogPr = tmp_model.logPr - model.logPr;

  mprintf('EM %d: logPr=%f, new_logPr=%f, delta_logPr=%f\n', ...
      model.t, model.logPr, tmp_model.logPr, difLogPr);

  if options.eps_logL >= difLogPr,
    model.exitflag = 1;
  else
    model = tmp_model;
    model.exitflag = 0;
  end

  // counter of iterations
  model.t = model.t + 1;

end // while main loop

return;

endfunction
