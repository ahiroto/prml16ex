function data = load_script(file)

Fid = mopen(file, 'r');
[N,X,Y] = mfscanf(%inf, Fid, '%s%f')
mclose(Fid);

numdata = length(Y);

data.X = cell(1,numdata);
data.y = Y'

for i = 1:numdata,
  Fid = mopen(X(i,1), 'r');
  [N2, X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,X11,X12,X13,X14,X15,X16,X17,X18,X19,X20,X21,X22,X23,X24,X25,X26] = mfscanf(%inf, Fid, '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f');
  data.X(1,i).entries = [X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,X11,X12,X13,X14,X15,X16,X17,X18,X19,X20,X21,X22,X23,X24,X25,X26]';
  mclose(Fid);
end

return;

endfunction
