function b = backward(lobsprob, model)
  
  ns = size(model.state,2); // Number of states
  nf = size(lobsprob,2);    // Number of frames
  
  b = -%inf * ones(ns,nf);
  b(ns,nf) = log(model.trans(ns,2)) + 0.0;
  min_ns = ns;
  for t=nf-1:-1:1,
    b(ns,t) = log(model.trans(ns,1)) + lobsprob(ns,t+1) + b(ns,t+1);
    if t <= nf-ns+1,
      min_ns = 1;
    else
      min_ns = min_ns -1;
    end
    for s=ns-1:-1:min_ns,
      sumLogPr = -%inf;
      sumLogPr = LAddS(sumLogPr, log(model.trans(s,2)) + lobsprob(s+1,t+1) + b(s+1,t+1));
      sumLogPr = LAddS(sumLogPr, log(model.trans(s,1)) + lobsprob(s,t+1) + b(s,t+1));
      b(s,t) = sumLogPr;
    end
  end
  
  return 

endfunction




