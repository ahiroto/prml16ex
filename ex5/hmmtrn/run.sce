// run.sce:
// sample program for word recognition using hidden Markov models
// trained by EM algotithm

clear;

exec('./mahalan.sci');
exec('./log_gauss.sci');
exec('./LAddS.sci');
exec('./mlchmm.sci');
exec('./melhmm.sci');
exec('./emhmm.sci');
exec('./calc_fb.sci');
exec('./forward.sci');
exec('./backward.sci');
exec('./load_script.sci');
exec('./cerror.sci');

//--------------------------------------------------
// Load feature parameters of five spoken words
// - trainlist.txt: training data list
// - recoglist.txt: test data list
//--------------------------------------------------
// trn/tst:
//  .X [cell(1,numdata)]: training data cell array
//  .y [1 x numdata]: class label
trn = load_script('./trainlist.txt');
tst = load_script('./recoglist.txt');

//--------------------------------------------------
// Training of HMM using ML estimation by Em algorithm
//
// Options:
//  .nstate: Number of States with pdf (default: 3)
//  .tmax: Maximum number of iterations (default: %inf)
//  .cov_type: Type of estimated covariance matrices
//  .verb: If 1 then processing details are displayed.
//--------------------------------------------------
model = cell(1,5);

options.nstate = 1;
options.tmax = 1;
options.eps_logL = 0;
options.cov_type = 'full';
// options.cov_type = 'diag';
options.verb = 1;

for i = 1:5,
  mprintf('training: model of %d\n',i);
  idx = find(trn.y == i);
  trnd = trn.X(1,idx); // [cell(1,length(idx))]
  model(1,i).entries = emhmm(trnd, options);
end

//--------------------------------------------------
// Classify testing data
//--------------------------------------------------
tstd = tst.X(1,:);

nd = size(tstd, 2);  // Number of data
ns = options.nstate; // Number of states

dfce = zeros(5,nd);
for n=1:nd,
  X = tstd(1,n).entries;
  [dim,nf] = size(X);
  for i =1:5,
    mdl = model(1,i).entries;
    lobsprob = log_gauss(X, mdl); // lobsprob: emmision prob. [nstate x nframe]
    a = forward(lobsprob, mdl);   // a: foward prob. [nstate x nframe]
    dfce(i,n) = a(ns,nf) + log(mdl.trans(ns,2));
  end
end

//--------------------------------------------------
// Evaluate testing error
//--------------------------------------------------
[max_value, max_idx] = max(dfce,'r');
error_rate = cerror(tst.y, max_idx)

//--------------------------------------------------
//          End: run.sce
//--------------------------------------------------

options.nstate
options.tmax
options.cov_type
error_rate
