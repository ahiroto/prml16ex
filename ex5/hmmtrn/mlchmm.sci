function model = mlchmm(trnd, options)
  
  if ~mtlb_isfield(options, 'cov_type'), 
    options.cov_type = 'full';
  end
  
  ndim = size(trnd(1,1).entries,1);
  
  acc = cell(1,options.nstate);
  // acc(1,s).entries : Accumulator for each state
  //  .Sum [ndim x 1]: Sum
  //  .SumSq [ndim x ndim]: Square sum
  //  .occ [1 x 1]: Occupation count
  for s=1:options.nstate,
    tg.Sum = zeros(ndim,1);
    tg.SumSq = zeros(ndim,ndim);
    tg.occ = 0;
    acc(1,s).entries = tg;
  end
  
  //--------------------------------------------------
  // * Statistics accumulation
  //--------------------------------------------------
  numdata = size(trnd,2);
  for n = 1:numdata,
    X = trnd(1,n).entries;
    [dim, numframe] = size(X);
    bnd = floor(numframe/options.nstate);
    sp = 1; ep = bnd;
    for s=1:options.nstate,
      if s == options.nstate,
        ep = numframe;
      end
      idx = sp:ep;
      
      tg = acc(1,s).entries;
      tg.Sum = tg.Sum + sum(X(:,idx),2);
      tg.SumSq = tg.SumSq + (X(:,idx) * X(:,idx)');
      tg.occ = tg.occ + length(idx);
      acc(1,s).entries = tg;
      
      sp = sp + bnd;
      ep = ep + bnd;
    end
  end
  
  //--------------------------------------------------
  // * Initial model computation
  //-------------------------------------------------- 
  model.state = cell(1,options.nstate);
  model.trans = zeros(options.nstate,2);
  
  for s=1:options.nstate,
    tg = acc(1,s).entries;
    state.Mean = tg.Sum / tg.occ;
    Cov = (tg.SumSq - tg.occ * state.Mean * (state.Mean)') / tg.occ;
    select options.cov_type,
      case 'full',
        state.Cov = Cov;
      case 'diag',
        state.Cov = diag(diag(Cov));
      else
        error('Wrong cov_type.');
    end
    model.state(1,s).entries = state;
    model.trans(s,1) = (tg.occ-1) / tg.occ; // same state transition
    model.trans(s,2) = 1 / tg.occ; // next state transition
  end
      
  model.cov_type = options.cov_type;
  model.fun = 'pdfgauss';
  
  return;
  
endfunction

