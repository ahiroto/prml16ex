function [fp, bp, logObsProb, logPr] = calc_fb(trnd, model)
  
  nd = size(trnd,2);
  
  fp = cell(1,nd);
  bp = cell(1,nd);
  logObsProb = cell(1,nd);
  logPr = zeros(1,nd);
  
  for n=1:nd,
    X = trnd(1,n).entries;
    [dim,nf] = size(X);
    lobsprob = log_gauss(X, model);
    
    a = forward(lobsprob, model);
    b = backward(lobsprob, model);
    
    logPrFp = a(ns,nf) + log(model.trans(ns,2));
    logPrBp = 0.0 + lobsprob(1,1) + b(1,1);
    temp = (logPrFp + logPrBp) / 2;
    //mprintf('logPr:%f\n',temp);
       
    fp(1,n).entries = a;
    bp(1,n).entries = b;
    logObsProb(1,n).entries = lobsprob;
    logPr(1,n) = temp;    
  end
  
  return
  
endfunction

