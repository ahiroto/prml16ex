function y = knnclass(X, model)
// KNNCLASS k-Nearest Neighbours classifier.
//
// Synopsis:
//  y = knnclass(X,model)
//
// Description:
//  The input feature vectors X are classified using the K-NN
//  rule defined by the input model.
//
// Input:
//  X [dim x num_data] Data to be classified.
//  model [struct] Model of K-NN classfier:
//   .X [dim x num_prototypes] Prototypes.
//   .y [1 x num_prototypes] Labels of prototypes.
//   .K [1x1] Number of used nearest-neighbours.
//
// Output:
//  y [1 x num_data] Classified labels of testing data.
//
// Example:
//  trn = load('riply_trn');
//  tst = load('riply_tst');
//  ypred = knnclass(tst.X,knnrule(trn,5));
//  cerror( ypred, tst.y )
//
// See also
//  KNNRULE.
//

// (c) Statistical Pattern Recognition Toolbox, (C) 1999-2003,
// Written by Vojtech Franc and Vaclav Hlavac,
// <a href="http://www.cvut.cz">Czech Technical University Prague</a>,
// <a href="http://www.feld.cvut.cz">Faculty of Electrical engineering</a>,
// <a href="http://cmp.felk.cvut.cz">Center for Machine Perception</a>

// Modifications:
// 19-may-2003, VF
// 18-sep-2002, V.Franc

//X=c2s(X);
//model=c2s(model);

if ~mtlb_isfield(model, 'K'),
  model.K = 1;
end;

//y = knnclass_mex(X, model.X, model.y, model.K);
trn_data = model.X;
trn_labels = model.y;
tst_data = X;
k = model.K;

[trn_dim, num_trn_data] = size(trn_data);
[tst_dim, num_tst_data] = size(tst_data);

dist = zeros(num_trn_data,num_tst_data);

for i = 1:num_trn_data
  temp = tst_data - trn_data(:,i) * ones(1,num_tst_data);
  for j = 1:num_tst_data
    t = temp(:,j);
    dist(i,j) = sqrt(t' * t);
  end
end

max_labels = zeros(k, num_tst_data);

for l=1:k
  [min_value, min_idx] = min(dist,'r');
  max_labels(l,:) = trn_labels(1,min_idx);
  dist(min_idx, :) = %inf;
end

count = zeros(1,k);

for i = 1:num_tst_data
  cl = max_labels(:,i);
  for l = 1:k
    count(1,l) = size(find(cl==cl(l,1)),2);
  end
  [max_value, max_idx] = max(count);
  y(1,i) = max_labels(max_idx,i);
end

return;

endfunction
