function y = log_pdfgmm(X, model)
  
  LZERO = -1.0E+50;
  num_data = size(X,2);
  
  newAlpha = log(model.Prior(:) * ones(1,num_data)) + log_pdfgauss(X, model);
  sumLogL = ones(1, num_data) * LZERO;
  for i = 1:options.ncomp,
    sumLogL = LAdd(sumLogL, newAlpha(i,:));
  end
  
  y = sumLogL;
  
  return
  
endfunction

