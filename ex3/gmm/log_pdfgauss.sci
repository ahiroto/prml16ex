function y = log_pdfgauss(X, arg1, arg2)
  
  // process input arguments
  if argn(2) < 3,
    Mean = arg1.Mean;
    Cov =  arg1.Cov;
  else
    Mean = arg1;
    Cov =  arg2;
  end
  
  // get dimensions
  [dim, num_data] = size(X);
  ncomp = size(Mean, 2);
  
  // alloc memory
  y = zeros(ncomp, num_data);
  
  // evaluate pdf for each component
  for i = 1:ncomp,
    dist = mahalan(X, Mean(:,i), Cov(1,i).entries);
    y(i,:) = -0.5 * dist - 0.5 * log((2*%pi)^dim * det(Cov(1,i).entries));
  end
  
  return;
  
endfunction

