function sumLogL = LAdd(LogL1, LogL2)
  
  idx1 = find(LogL1 > LogL2);
  idx2 = find(LogL1 < LogL2);
  
  sumLogL(1,idx1) = LogL1(1,idx1) + log(1 + exp(LogL2(1,idx1) - LogL1(1,idx1)));
  sumLogL(1,idx2) = LogL2(1,idx2) + log(1 + exp(LogL1(1,idx2) - LogL2(1,idx2)));
  
  return
  
endfunction

