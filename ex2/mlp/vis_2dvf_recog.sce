getf('c:\work\scilab\ex02\load_2dvf.sci');

// Load test data
// 'recog_5vf.txt': vowel formants with their class labels
tst = load_2dvf('c:\work\scilab\ex02\recog_5vf.txt');

tst_a = tst.X(:,find(tst.y==1)); // "A"
tst_i = tst.X(:,find(tst.y==2)); // "I"
tst_u = tst.X(:,find(tst.y==3)); // "U"
tst_e = tst.X(:,find(tst.y==4)); // "E"
tst_o = tst.X(:,find(tst.y==5)); // "O"

// Visualization
plot(tst_a(1,:), tst_a(2,:), 'bx');
plot(tst_i(1,:), tst_i(2,:), 'rx');
plot(tst_u(1,:), tst_u(2,:), 'gx');
plot(tst_e(1,:), tst_e(2,:), 'kx');
plot(tst_o(1,:), tst_o(2,:), 'mx');
legend('A','I','U','E','O');
xtitle('1st formants (f1) and 2nd formants (f2) of five vowels','f1', 'f2');

// End: visual_2dvf_recog.sci

