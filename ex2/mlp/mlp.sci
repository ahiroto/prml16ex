function model = mlp(data, options, init_model)
// Multi Perceptron algorithm with sigmoid function to train binary
//linear classifier.
//
// Synopsis:
//  model = perceptron(data)
//  model = perceptron(data,options)
//  model = perceptron(data,options,init_model)
//
// Input:
//  data [struct] Labeled (binary) training data.
//   .X [dim x num_data] Input vectors.
//   .y [1 x num_data] Labels (1 or 2).
//
//  options [struct]
//   .tmax [1x1] Maximal number of iterations (default tmax=inf).
//     If tmax==-1 then it does not perform any iteration but returns only
//     index of the point which should be used to update linear rule.
//
//  init_model [struct] Initial model; must contain items
//    .W, .b and .t (see above).
//
// (C) 2014, Written by Naohiro Tawara (tawara@pcl.cs.waseda.ac.jp)
//

[dim, num_data] = size(data.X); // dimension and num of data

// Process input arguments
// -----------------------------
if argn(2) < 2,
  options = [];
end

if ~mtlb_isfield(options,'tmax'),
  options.tmax = %inf;
end
if ~mtlb_isfield(options,'eta'),
  options.eta = 0.01;
end
if ~mtlb_isfield(options,'hdidden'),
  options.nhidden = 10;
end

eta        = options.eta;
num_hidden = options.nhidden;
tmax       = options.tmax;

if argn(2) < 3,
  // create init model
  model.W1 =  -1 + rand(dim, num_hidden) * 2;
  model.b1 =  -1 + rand(1,   num_hidden) * 2;
  model.W2 =  -1 + rand(num_hidden, 1)   * 2;
else
  // take init model from input
  model = init_model;
end

if ~mtlb_isfield(model,'t'),
  model.t = 0;
end

model.exitflag    = 0;
model.last_update = 0;

// augument input vector
// Add one constant coordinates to the data
dim    = dim + 1;
data.X = [data.X; ones(1,num_data)];


// main loop
// -----------------------------------
olderr = %inf;
while tmax > model.t & model.exitflag == 0

    // Forward Propagation
    model.H = logistic([model.W1; model.b1]' * data.X);
    model.Y = logistic([model.W2]' * model.H);

    // Error Back Propagation
    // Calculating deltas
    delta2 = (data.y-model.Y) .* model.Y .* (1 - model.Y);
    delta1 = repmat(delta2, num_hidden, 1)
             .* repmat(model.W2, 1, num_data) .* model.H .* (1 - model.H);


    // Updating parameters
    model.W2 = model.W2 + eta/num_hidden .* (delta2 * model.H')';
    model.W1 = model.W1 + eta/num_hidden .* (delta1 * data.X(1:2,:)')' ;
    model.b1 = model.b1 + eta/num_hidden .* (delta1 * data.X(3,:)')' ;

    model.t = model.t + 1;
    err = sum(abs(model.Y-data.y)) ./num_data;
    delta_err = olderr - err;
    printf('%d: Error: %f(delta: %f)\n',model.t, err, delta_err);
    if(delta_err<0.0001) then
        model.exitflag = 1;
    end
    olderr = err;
end

endfunction
