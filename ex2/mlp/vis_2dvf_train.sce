getf('c:\work\scilab\ex02\load_2dvf.sci');

// Load training data
// 'train_5vf.txt': vowel formants with their class labels
trn = load_2dvf('c:\work\scilab\ex02\train_5vf.txt');

trn_a = trn.X(:,find(trn.y==1)); // "A"
trn_i = trn.X(:,find(trn.y==2)); // "I"
trn_u = trn.X(:,find(trn.y==3)); // "U"
trn_e = trn.X(:,find(trn.y==4)); // "E"
trn_o = trn.X(:,find(trn.y==5)); // "O"

// Visualization
plot(trn_a(1,:), trn_a(2,:), 'bo');
plot(trn_i(1,:), trn_i(2,:), 'ro');
plot(trn_u(1,:), trn_u(2,:), 'go');
plot(trn_e(1,:), trn_e(2,:), 'ko');
plot(trn_o(1,:), trn_o(2,:), 'mo');
legend('A','I','U','E','O');
xtitle('1st formants (f1) and 2nd formants (f2) of five vowels','f1', 'f2');

// End: visual_2dvf_recog.sci

