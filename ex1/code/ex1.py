import numpy as np
from matplotlib import pyplot as plt
from sklearn import decomposition

#平均
mu1 = [5,4]
mu2 = [4,5]
#共分散
cov = [[1,0.9],[0.9,1]]
 
#100はデータ数
x1,y1 = np.random.multivariate_normal(mu1,cov,100).T
x2,y2 = np.random.multivariate_normal(mu2,cov,100).T

N1 = np.c_[x1, y1]
N2 = np.c_[x2, y2]
N3 = np.r_[N1, N2]

dim = 1
pca = decomposition.PCA(dim)
pca.fit(N3)
Upca = pca.components_.T

print(Upca)

# 主成分分析後のサイズ

#グラフ描画
#背景を白にする
plt.figure(facecolor="w")
 
# plot axes
#散布図をプロットする
plt.scatter(x1,y1,color='r',marker='x',label="Class-1")
plt.scatter(x2,y2,color='b',marker='.',label="Class-2")

pc_line = np.array([1, 8]) * (Upca[1][0] / Upca[0][0])
plt.plot([1, 8], pc_line, 'g')

#グラフの中に文字を入れてみる
plt.figtext(0.6,0.3,"$N_1$",size=20)
plt.figtext(0.3,0.6,"$N_2$",size=20)
 
#ラベル
plt.xlabel('$x$',size=20)
plt.ylabel('$y$',size=20)

#軸
plt.axis([0,9,0,9.0],size=20)
plt.grid(True)
plt.legend(loc='lower right')
 
#保存
#plt.savefig("multivariate_normal.png",format = 'png', dpi=200)
plt.show()
plt.close()
