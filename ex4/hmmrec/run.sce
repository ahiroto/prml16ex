// hmmrec/run.sce:
// sample program of isolated spoken word recognition using hidden Markov models
// trained by EM algotithm

clear;

exec('./mahalan.sci');
exec('./log_gauss.sci');
exec('./LAddS.sci');
exec('./forward.sci');
exec('./viterbi.sci');
exec('./load_script.sci');
exec('./cerror.sci');

//--------------------------------------------------
// Load feature parameters of five spoken words
// - recoglist.txt: test data list
//--------------------------------------------------
// tst:
//  .X [cell(1,numdata)]: test data cell array
//  .y [1 x numdata]: class label
tst = load_script('./recoglist.txt');

// load HMM from binary data -> model
load('hmms');

//--------------------------------------------------
// Classify testing data
//--------------------------------------------------
tstd = tst.X(1,:);

nd = size(tstd, 2);                    // Number of data
ns = size(model.entries(1).trans,'r'); // Number of states

dfce = zeros(5,nd);
for n=1:nd,
  X = tstd(1,n).entries;
  [dim,nf] = size(X);
  for i =1:5,
    mdl = model(1,i).entries;
    lobsprob = log_gauss(X, mdl); // lobsprob: emmision probability [nstate x nframe]
    alpha = forward(lobsprob, mdl);   // a: foward prob. [nstate x nframe]
    // alpha = viterbi(lobsprob, mdl);   // a: viterbi prob. [nstate x nframe]
    dfce(i,n) = alpha(ns,nf) + log(mdl.trans(ns,2));
  end
end
// disp(dfce);

//--------------------------------------------------
// Evaluate testing error
//--------------------------------------------------
[max_value, max_idx] = max(dfce,'r');
disp(max_idx);
disp(max_value);
error_rate_IKIOI = cerror(tst.y, max_idx, 1)
error_rate_IYOIYO = cerror(tst.y, max_idx, 2)
error_rate_URAYAMASHII = cerror(tst.y, max_idx, 3)
error_rate_OMOSHIROI = cerror(tst.y, max_idx, 4)
error_rate_GUAI = cerror(tst.y, max_idx, 5)
error_rate_all = cerror(tst.y, max_idx)

//--------------------------------------------------
//          End: hmmrec/run.sce
//--------------------------------------------------
